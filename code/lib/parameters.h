#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <iostream>
#include <fstream>
#include <string>

#include <errhand.h>
#include <utilis.h>
#include <global.h>

using namespace std;

#define errhand_macro(err) errhand(err, __FILE__, __LINE__)

class parameters{
    
    
public:
    parameters(){}
    ~parameters(){}

    void read_param();
    
private:
    ifstream in;
    string value;
    bool err;
    utilis utls;
    
    
    inline void read_line(ifstream *input_file, string *read_value){

        string line;
        istringstream stream;

        getline(*input_file, line);
        stream.str(line);
        stream >> *read_value;
        stream.clear();

        return;
    }

    
};


#endif
