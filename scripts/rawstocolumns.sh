LINE=28 #line where the star is
head -n $LINE mhe.dat | tail -n 1 > heline
head -n $LINE mco.dat | tail -n 1 > coline
head -n $LINE mass.dat | tail -n 1 > massline
head -n $LINE radius.dat | tail -n 1 > radline
head -n $LINE time.dat | tail -n 1 > timeline
head -n $LINE lumi.dat | tail -n 1 > lumline

awk '{for (i=1;i<=NF;i++) print $i}' heline > he
awk '{for (i=1;i<=NF;i++) print $i}' coline > co
awk '{for (i=1;i<=NF;i++) print $i}' massline > mass
awk '{for (i=1;i<=NF;i++) print $i}' timeline > time
awk '{for (i=1;i<=NF;i++) print $i}' radline > rad
awk '{for (i=1;i<=NF;i++) print $i}' lumline > lum

paste time mass he co rad lum > all.dat

rm -rf time mass he co rad radline massline coline heline timeline lumline lum
