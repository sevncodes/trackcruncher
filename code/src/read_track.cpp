#include <read_track.h>
#include <global.h>

void readtrack::get_track(const string fname){
    
    
    
    //open the track file you want to read
    in.open(fname.c_str(), ios::in);
    if(!in){ 
        string errmsg = "Cannot read file " + fname;
        throw errhand_macro(errmsg);
    }
    
    
    
    //ignore the first 277 lines... 
    //they contain preliminary information on the track
    //number of lines tu ignore is n_ignore (it is an input parameter)
    
    //the first line must be that containing the names of the columns
    
    //cout<<" n ignore = "<<n_ignore<<endl;
    for(int i = 1; i < n_ignore; i++){
        getline(in, line);
        //std::cout<<"W "<<line<<std::endl;
    }

    
    
    //get correspondence between arrays, columns and indexes
    istringstream stream;
    double double_val;
    string string_val;
    read_line(&in, &stream); //this should be the header
   
//   cout<<stream.str()<<endl; 
    for(;;){

        if(!read_header(&stream, &string_val, __FILE__, __LINE__)) break;

        //if(!read_header(&stream, &string_val, __FILE__, __LINE__)){
        //        cout<<" read/expected column = "<<i<<"/"<<n_columns<<endl;
        //        throw errhand_macro("Mismatch between the number of expected columns and the number of read columns");
        //}
        
        
        if(global::stevocode == "parsec" || global::stevocode == "parsec_old")
            get_pointers_parsec(string_val);
        else if(global::stevocode == "franec")
            get_pointers_franec(string_val, 0);
        else if(global::stevocode == "mist")
            get_pointers_mist(string_val);
        else
            throw errhand_macro("Unrecognized stellar evolution code");
        
    }
    

    //exit(1);
    // if(global::stevocode == "parsec_old" || global::stevocode == "mist") n_lines = n_lines - n_ignore;
    //remove header, n_ignore defined in parameters.txt file
    n_lines = n_lines - n_ignore;
    //remove footer
    //if(global::stevocode == "parsec_old") n_lines -= 2;
    //std::cout<<"NIG "<<n_ignore<<std::endl;


    //format of input file handled with pointers (see pointers_array)
    for(int i = 0; i < n_lines; i++){

        //cout<<" nline = "<<i<<endl;
        read_line(&in, &stream);

        if(in.eof()) {
            throw errhand_macro("Unexpected end of file. Mismatch between number of lines (var:n_lines) and actual lines");
        }
        
        for(int k = 0; k < pointers_array.size(); k++){

            if(!read_value(&stream, &double_val, i, k, __FILE__, __LINE__)){
                cout<<" read column = "<<k+1<<endl;
                throw errhand_macro("Mismatch between the number of expected columns and the number of read columns");
            }


            
            if(pointers_array[k] != nullptr){
                pointers_array[k]->push_back(double_val); //this will automatically go in the right place of the array global:data.
            }
            
        }

    }
    
    
    //cout<<" end cycle "<<endl;
    //CHECK IF ALL THE ARRAYS ARE LOADED... CHECK THEIR DIMENSION.
    
    int dim1 = static_cast<int>(global::data.mass.size());
    int dim3 = static_cast<int>(global::data.age.size());

    //cout<<" SIZES = "<<global::data.mass.size()<<endl;
    
    if(dim1 != dim3)
        throw errhand_macro("Mismatch in the dimension of the arrays read from the input track file");


    ///Update other values based on tracks
    for (int i=0; i < global::data.age.size(); i++){

        if (global::stevocode == "parsec" or global::stevocode == "parsec_old")
            complete_table_parsec(i);
        else if (global::stevocode == "mist")
            complete_table_mist(i);
        else if (global::stevocode == "franec")
            complete_table_franec(i);

    }
    
    clean_up();

    global::calledTrack = true;
    
    
    return;
    
}


void readtrack::clean_up(){


    std::set<int> bad_idx = find_bad_rows();


    for (auto& table : global::data.all_tables){

        if (!table->empty())
            filter_tab_by_id(table,bad_idx);
    }


    /**
    if(global::stevocode == "parsec" || global::stevocode == "parsec_old"){
        int dimension = static_cast<int>(global::data.age.size());
        
        //remove values for AGE <= 0.2
        int count = 0;
        
        for(int i = 0; i < dimension; i++){
            if(global::data.age[i] <= 0.2)
                count++;
        }
        
        
        for(int k = 0; k < pointers_array.size(); k++){
            if(pointers_array[k] != nullptr){
                pointers_array[k]->erase(pointers_array[k]->begin(), pointers_array[k]->begin()+count);
            }
        }
        
        cout<<" end clean up "<<endl;
    }
    **/
    
    return;
    
}

std::set<int> readtrack::find_bad_rows(){

    std::set<int> bad_idx;

    int dimension = static_cast<int>(global::data.age.size());


    std::cout<< " *Removing bad rows in the track file"<<std::endl;


    for(int i = 0; i < dimension; i++){


        //Age check, only parsec
        if( (global::data.age[i] <= 0.2) and (global::stevocode == "parsec" or global::stevocode == "parsec_old")) {
            bad_idx.insert(i);
            std::cout<< "   - Removing idx "<<i<<" because of data.age<=0.2 (Parsec and Parsec old)"<<std::endl;
        }
        //Mass check
        else if (global::data.mass[i]<global::data.mcoreco[i]){
            bad_idx.insert(i);
            std::cout<< "   - Removing idx "<<i<<" because M<MHE"<<std::endl;
        }
        else if (global::data.mcorehe[i]<global::data.mcoreco[i]){
            bad_idx.insert(i);
            std::cout<< "   - Removing idx "<<i<<" because MHE<MCO"<<std::endl;
        }


    }

    return bad_idx;



}
