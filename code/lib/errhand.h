#ifndef ERRHAND_H
#define ERRHAND_H

#include <iostream>

#include <string>
#include <sstream>

#ifdef TELEGRAM
#include <bot_alert.h>
#endif

using namespace std;

class errhand{

  public:
      errhand(){}; //overloaded constructor
      
    errhand(string err_msg_input, const char* file_input, int line_input)
    {
		 err_msg = err_msg_input;
		 file    = to_string_loc(file_input);
		 line    = line_input;
         
         print_to_stdout();
         invoke_telegram_bot();
	 }
	 	 
	 string get_msg()  const {return err_msg;}
	 string get_file() const {return file;   }
	 int    get_line() const {return line;   }

      
      string to_string_loc(const char* obj){
         ostringstream out;
         out << obj;
         return out.str();
      }
	 
	 
	 inline void invoke_telegram_bot(){

#ifdef TELEGRAM
     
		 bot = new bot_alert;
         string message = "<b>An error occurred</b>\n<b>Message</b> = " + get_msg() + "\n" + "<b>File</b> = " + get_file() + "\n" + "<b>Line</b> = " + bot->to_string_loc(get_line()) + "\n";
         
         bot->init();
         bot->send_message(message);   
         bot->send_sticker("sad");
#endif

         
		 return;
	 }
	 
	 
	 inline void print_to_stdout(){
    
         cerr<<" An error occurred: "<<endl;
         cerr<<"          Message ----> "<<get_msg()<<endl;
         cerr<<"          File    ----> "<<get_file()<<endl;
         cerr<<"          Line    ----> "<<get_line()<<endl;
    
        return;
         
    }



  private:
	 string   err_msg, file;
	 int      line;
#ifdef TELEGRAM
     bot_alert *bot;
#endif

};


#endif
