#ifndef UTILIS_H
#define UTILIS_H

#include <iostream>
#include <fstream>
#include <errhand.h>

using namespace std;

#define errhand_macro(err) errhand(err, __FILE__, __LINE__)

class utilis{
    
    
public:
    utilis(){}
    ~utilis(){}

    template <class T> T string_to_number(string str, bool *err){
    T value;
    *err = false;
    
    stringstream stream(str);
    stream >> value;
    
    if(stream.fail())
        *err = true;
    
    
    return value;
}
    

    
    
private:
    ifstream in;
    string value;

};

#endif
