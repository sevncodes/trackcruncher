# TrackCruncher

TrackCruncher is a suite of codes and scripts to process stellar tracks to produce tables SEVN population-synthesis code. In addition (or in alternative) the TrackCruncher interpolator can be used or to reduce the memory weight of stellar tracks.

At the moment it is optimised to process the outputs of PARSEC (Bressan et al., 2012), FRANEC (Liming & Chieffi 2018) and MIST (Choi et al. 2016), but it can be easily extended to process the output of other stellar evolution codes.


Based on the original code developed by Mario Spera

Current developers: Giuliano Iorio, Guglielmo Costa, Mario Spera
contacts: giuliano.iorio.astro@gmail.com


# Basic user guide

## Compile

TrackCruncher use cmake to automatise the compilation.
1. Create a build directory inside the code folder (remove the build directory if already present) and enter in the directory `cd code; rm -rf build; mkdir build; cd build`
2. Initialise Cmake `cmake ..`
3. If everyting went right, compile using make or make -h `make -j`
4. Two executable (track.x and track_pureHE.x) will be created in the home directory.

## Run

### Direct run

The two executables can be run with the following formula

`./track.x (./track_pureHE.x) -l <NUMEBER OF LINE TO READ>  -f <PATH TO INPUT TABLE> -prev 6`

- the -l input indicates the total number of line contained in the file
- the -f input is the path ot the track file to interpolate
- the -prev input is not used anymore and in future release will be removed, so it is ok to put 6 here

There is an additional requirement needed to run the interpolator.  A folder called _input_ has to be created
in the same path where track.x or track_pureHE.x are. Inside the folder a text file _parameter.txt_ has to be created.
Inside parameter.txt the following data (one per line) need to be included:

- ii   //lines to ignore in the input track file (parsec:1, parsec_old:277,  franec:1, mist:12)
- dd.dd  //Maximum relative error between interpolation and tracks default 0.02
- ii    //Number of decimal digits in output, default 8
- ss //parsec or franec or parsec_old or mist
- ss  //AGB_cut options (yes or no)

Inside the folder script/input_example there are a number of ready to use parameter.txt files

The output of completed run is a number of xx_line.dat files (the number of files can change depending on the code) where xx is the number  of the interpolated properties (e.g. mass_line.dat). Each line contains just one row a variable number of columns following the evolution
of the properties.




### Run with script

The single command line run is not very useful if one wants to produce a complete set of SEVN tables or to reduce the dimension of a set of stellar tracks
In the  scripts folder there are a number of ready to use scripts to automatise the recursive call of the interpolator co make a complete set  of SEVN table starting from a complete set of tracks.
The format of the input tables has to strictly follow these rules:
- A parent folder (its folder path is the INPUT parameter)
- A series of child folders for each metallicity, It is preferable to star the name of this folder with Z followed by the Z-metallicity (not [Fe/H]). The script assumes that the first value of the metallicity is always 0, and all other the other values are decimals.
- The script makes an order of the files based on this assumption, if this is not true, the final order in output
        could be not sorted in zams mass, BE CAREFUL!


    e.g.

```
TRACKS  
|
|---Z0.002
|     |-- M010.tab
|     |-- M020.tab
|     |-     ...
|   
|---Z0.02
|    |--  M010.tab
|    |--  M020.tab
|    |     ...
|__ ...
```

#### Python script (recommended)
The script make_tables is a fully automatised  Python script (only working with Python3).
It is the same script for all the codes and can be used with both the executable track.x and track_pureHE.x and it is not needed to make the input folder (it is made by the script itself).
The only thing to modify before to run it are a number of parameters at the beginning of the script:


```
CODE="parsec" #Name of the code used to generate the tracks
EXE="track.x" #COmplete path to the executable (can be both track.x or track_pureHE.x)
NDIGIT=8.  #same of parameter.txt see above
MAXIMUM_RELATIVE_ERROR=0.02 #same of parameter.txt see above
AGB_CUT=True  #same of parameter.txt see above but using True or False
INPUT= None  #Complete PATH to the INPUT folder
OUTPUT="output" #PATH of the output folder
```

The INPUT parameter can be equal to NONE, in that case the path to the input folder has to be given in the command line  
`./make_tables <PATH_TO_INPUT>`
othrwise it can be called just as
`./make_tables`

Other info about the script can be found directly in the documentation section of the script


**NOTICE:** Remember to make the script executable using `chmod u+x make_tables`


#### Bash script
In the script folder there are a number of bash script already made for a given code (e.g. Parsec or Mist). In order to use these script
the input folder with the parameter.txt is needed as in the case of the direct run. The first rows of the script have to be modified to insert
the name of the input folder.  Be careful to change the DIR path in the first row with the path to the track folder. Accordingly change the
`row v2=${DIR:x} #remove the first N characters  in the string of the directory using the right x, where x is the number of characters of the path used in the first row (including )`.  Finally change the row  `v2=${v2//yyyy} #remove the yyyy text identifier where yyyy is the prefix of each metallicity folder in the tracks folder non including _` . For example if the folder are called PUREHE_36_Z002, yyyy=PUREHE36.


To run a given script just use `./script_name.sh`
The output will be save in  a folder named output by default.


## Table validator 
Once the SEVN tables have been produced by TrackCruncher, we suggest to 
Process the tables with the script tables_validator.py in the script folder.
The script can be used simply as 

python tables_valitor.py <TABLES_PATH>  --plot[OPTIONAL] --purehe[Optional]

The script analyses all the tables and check if they are SEVN compliant. 
All the information are printed to the standard output

- If the option plot is include the script also produces a number of diagnostic and summary plots 
- For  table of pureHe stars remember to include the option --purehe


