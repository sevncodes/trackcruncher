#include <table.h>
#include <vector>
#include <algorithm>

void table::build(){


    bool next_iteration = false;

    //Initialise Maxindex to 0
    maxindex = 0;
    //Initialise maxindex co finding the indexes of the maximum value of MCO
    set_maxco_index();
    //Initialise maxindex YCEN finding the indexes of the maximum value of YCEN
    set_maxYCEN_index();

    //max(MCO)<=0 means that the track is not complete, so exit here
    if (data.mcoreco[maxindex_co_first]<=0){
        cout << " ******** This track does not have a CO core" << endl <<endl;
        throw errhand_macro("This track is not complete. Not writing the track. File: "+
                            global::filename);
    }

    std::cout<<" *CALCULATING PHASES"<<std::endl;
    calculate_all_phases(); //Notice we calculate phases but also set maxindex (that can depends on AGB stop)
    write_phases();



    int dimension = maxindex == 0 ? data.mass.size()-1 : maxindex;
    //maxindex is the last model in the track.. the one that ignites CO!! We truncate all tracks here...
    //this is where I truncate the track, based on phases
    cout<<" Max index based on stop conditions is = "<<maxindex<<endl;
    cout<<" Max index based on tracks size is = "<<data.mass.size()-1<<endl;


    std::cout<<" *BUILDING TABLES"<<std::endl;

    //write zams mass in the listmass.dat file
    ofstream outlist;
    outlist.open("listmass.dat", ios::app);

    cout<<data.mass.size()<<endl;
    outlist<<data.mass[0]<<endl;

    outlist.close();


    int tab = 0;
    write_tables(tab); //first point always in tables


    bool co_decrases_stop=false; //Flag to check if the interpolation has benn stopped by a
    for(int i = 2; i < dimension; i++){

        ///Check if we have to skip this point a priori
        //We write table with 8 number of digit after the ., therefore in order  to avoid to have the same
        //time in the final table we have to a priori skip point for which the age diff with respect of the last
        //written point is less than our precision. NOTICE that the points are written inside check_variation and that the point
        //written is always i-1. Therefore we check the difference in age between i-1 and tab rather than i and tab.
        int orderofmag = (int)log10(data.age[i]);
        double minimum_dt=pow(10,-global::ndigit)*pow(10.0, orderofmag);
        if(i!=0 and  ((data.age[i-1] - data.age[tab]) < minimum_dt)){continue;}
        //Check masses (do not skip, so far we have to discuss)
        if (i!=0 and data.mass[i-1]<data.mcorehe[i-1]){
            std::cout<<" /*\\ WARNING: Mass("<<data.mass[i-1]<<")<MHE("<<data.mcorehe[i-1]<<") at position "
            <<i-1<<" /*\\"<<std::endl;
            //continue;
        }
        if (i!=0 and data.mcorehe[i-1]<data.mcoreco[i-1]){
            std::cout<<" /*\\ WARNING: MHE("<<data.mcorehe[i-1]<<")<MCO("<<data.mcoreco[i-1]<<") at position "
                     <<i-1<<" /*\\"<<std::endl;
            //continue;
        }
        ///////////////////////////////



        const double inv_dt = 1.0/(data.age[i] - data.age[tab]);

        check_wr_star(i);


        //check if CO is decreasing.... if this is the case just write the last CO point and return
        if(check_if_co_decreases(i, tab)){
            co_decrases_stop=true;
            std::cout<<" Stop due to the decreasing CO"<<std::endl;
            return;
        }
        //Else if the penultimate point is the first point of Maximum CO always write it
        else if (i-1==maxindex_co_first){
            write_tables(i-1);
            tab = i-1;
            continue;
        }



        //check mass variation
        next_iteration = check_variation(&data.mass[0], data.mass[tab], data.mass[i], data.age[tab], inv_dt, tab+1, i, &tab);
        if(next_iteration) {   continue;}
        
        //check luminosity variation
        next_iteration = check_variation_pow(&data.logl[0], pow(10.0, data.logl[tab]), pow(10.0, data.logl[i]), data.age[tab], inv_dt, tab+1, i, &tab);
        if(next_iteration) {   continue;}

        //check radius variation
        next_iteration = check_variation(&data.r[0], data.r[tab], data.r[i], data.age[tab], inv_dt, tab+1, i, &tab);
        if(next_iteration){   continue;}

        //check qHE variation
        next_iteration = check_variation(&data.qhe[0], data.qhe[tab], data.qhe[i], data.age[tab], inv_dt, tab+1, i, &tab);
        //cout<<data.qhe[i]<<endl;
        if(next_iteration) {   continue;}

        //check qCO variation
        //In check_if_co_decreases we already checked if co decreas below a certain tshold and in case we stop
        //Here we want just to check if the i-1 qco point is smaller than the  qco tab in that cases we
        // just skip the check (as done for time resolution above) because we don't want to record this value
        // (if it is the only want that is requiring to record it), remember in check_variation, if it is required
        // we  record the i-1  point.
        if (i!=0 and data.mcoreco[i-1]<data.mcoreco[tab]) continue;
        next_iteration = check_variation(&data.qco[0], data.qco[tab], data.qco[i], data.age[tab], inv_dt, tab+1, i, &tab);
        if(next_iteration) {   continue;}

    }

    ///Print last points
    //Remember tab is the index of the last point written that is surely < dimension and data.size-1
    double orderofmag;



    //If dimension is different  the length of the track save the stopping point
    if (dimension<data.mass.size()-1){
        orderofmag = (int)log10(data.age[dimension]);
        //if the time resolution is very small just use the smallest possible resolution in dt for the last point
        if(data.age[dimension] - data.age[tab] <= pow(10,-global::ndigit)*pow(10.0, orderofmag)) data.age[dimension] = data.age[tab] + pow(10,-global::ndigit)*pow(10.0, orderofmag);

        tab = dimension;
        check_wr_star(tab);//Here the HE core can be artificially reduced
        write_tables(tab);
    }


    //Write the last point
    std::cout<<" *WRITING LAST POINT"<<std::endl;
    write_last(tab);


    //File written
    write_endl();
    
    return;
    
}


void table::write_tables_last_agb(const int k){

    //NOW special treatment for star stopped by agb (see table::calculate_all_phases).
    //We want the last Mtot is the mass after the AGB phase, we approximate it as Mtot=1.02*MHE
    data.mass[k] = std::min(data.mass[k],1.02*data.qhe[k]*data.mass[k]);

    //Check if  MHE=Mass, put RHE=R
    if (data.mcorehe[k]==data.mass[k] and !data.rHE.empty()){
            data.rHE[k]=data.r[k];
    }

    //Inside write_tables MHE=qhe*Mtot and MCO=qco*Mtot, since we fixed mtot by hand we have to change
    //accordingly qhe and qco to preserve MHE and MCO.
    data.qhe[k] = data.mcorehe[k]/data.mass[k]; //MHE/Mtot
    data.qco[k] = data.mcoreco[k]/data.mass[k]; //MCO/Mtot
    write_tables(k);


}


void table::write_tables(const int k){


    elements++;
    
    ofstream out;


    if(global::stevocode == "parsec" || global::stevocode == "parsec_old"){


        out.open("mass_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<data.mass[k]<<"   ";
        out.close();


        if(global::stevocode == "parsec"){
            out.open("qconv_line.dat", ios::app);
            out<<scientific<<setprecision(global::ndigit);
            out<<data.QCNV[k]<<"   ";
            out.close();
        }


        out.open("depthconv_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        double dpt_out = data.DPTH_CNV[k]==0 ? 0 : pow(10,data.DPTH_CNV[k])/data.r[k];
        if (dpt_out>1) dpt_out=1.0; //Just to avoid round-off errors with fractions larger than 1
        out<<dpt_out<<"   "; // fraction of convective envelope in terms of total radius
        out.close();
    
        out.open("mco_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<data.qco[k]*data.mass[k]<<"   ";
        out.close();

        out.open("tconv_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        double tconv_out = data.TCONV[k]==0 ? 0 : pow(10,data.TCONV[k]);
        out<<tconv_out<<"   "; //Parsec tconv is in log(yr)
        out.close();

        out.open("hsup_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<data.XHS[k]<<"   ";
        out.close();

        out.open("hesup_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<data.XHES[k]<<"   ";
        out.close();

        out.open("csup_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<data.XCS[k]+data.XC13S[k]<<"   "; //C12 + C13
        out.close();

        out.open("osup_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<data.XOS[k]<<"   "; 
        out.close();

        out.open("nsup_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<data.XNS[k]<<"   "; //C12 + C13
        out.close();

        out.open("inertia_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<pow(10.0, log10(data.mIn[k])-2.0*log10(6.96e10)-log10(1.98892e+33))<<"   "; //in solar units!! (Rsun^2 Msun)
        out.close();
    
        out.open("mhe_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<data.qhe[k]*data.mass[k]<<"   ";
        //cout<<data.qhe[k]<<"   "<<data.qhe[k]*data.mass[k]<<endl;
        out.close();
    
        out.open("radius_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<data.r[k]/6.96e10<<"   ";
        out.close();
    
        out.open("time_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<(data.age[k]-data.age[0])/1.0e6<<"   ";
        out.close();
    
        out.open("lumi_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<pow(10.0, data.logl[k])<<"   ";
        out.close();
         
        out.open("rhe_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<data.rHE[k]/6.96e10<<"   ";
        //GI 10/04/21: Now rHE is assumed to be always in lin
        //if(data.rHE[k] != 0.0)
        //    out<<pow(10.0, data.rHE[k])/6.96e10<<"   ";
        //else
        //    out<<"0.0"<<"   ";
        out.close();
    
        out.open("rco_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<data.rCO[k]/6.96e10<<"   ";
        //if(data.rCO[k] != 0.0)
        //    out<<pow(10.0, data.rCO[k])/6.96e10<<"   ";
        //else
        //    out<<"0.0"<<"   ";
        out.close();

    }




    if(global::stevocode == "mist"){
    
        out.open("mass_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<data.mass[k]<<"   ";
        out.close();  

        out.open("mco_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<data.mcoreco[k]<<"   ";
        out.close();

        out.open("hsup_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<data.XHS[k]<<"   ";
        out.close();

        out.open("hesup_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<data.XHES[k]<<"   ";
        out.close();

        out.open("csup_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<data.XCS[k]<<"   "; //C12 only
        out.close();

        out.open("osup_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<data.XOS[k]<<"   ";
        out.close();

        out.open("nsup_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<data.XNS[k]<<"   ";
        out.close();

        out.open("mhe_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<data.mcorehe[k]<<"   ";
        out.close();
            
        out.open("radius_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<pow(10.0, data.logr[k])<<"   ";
        out.close();
    
        out.open("time_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<(data.age[k]-data.age[0])/1.0e6<<"   ";
        out.close();
    
        out.open("lumi_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<pow(10.0, data.logl[k])<<"   ";
        out.close();

    }
    
    else if (global::stevocode == "franec"){
    
        out.open("mass_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<data.mass[k]<<"   ";
        out.close();
        
        out.open("mco_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        cout<< " Mco " << data.mcoreco.at(k)<<std::endl;
        out<<data.mcoreco[k]<<"   ";
        out.close();
        
        out.open("mhe_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<data.mcorehe[k]<<"   ";
        out.close();
        
        out.open("radius_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<pow(10.0, data.logr[k])/6.96e10<<"   ";
        out.close();    
        
        out.open("time_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<(data.age[k]-data.age[0])/1.0e6<<"   ";
        out.close();
            
        out.open("lumi_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<data.logl[k]<<"   ";
        out.close();
            
        out.open("rhe_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<data.rHE[k]<<"   ";
        out.close();
        
        out.open("rco_line.dat", ios::app);
        out<<scientific<<setprecision(global::ndigit);
        out<<data.rCO[k]<<"   ";
        out.close();
    
    }
    
    
    return;
    
}


//next_iteration = check_variation(&data.mass[0], data.mass[tab], data.mass[i], data.age[tab], inv_dt, tab+1, i, &tab);
//if(next_iteration) continue;

bool table::check_variation(double *a, const double v0, const double v1, const double t0, const double inv_t1_minus_t0, const int start, const int end, int *tab){
    
    bool iteration = false;
    
    //check mass variation
        const double slope = (v1 - v0)*inv_t1_minus_t0;
        const double intercept = v0 - slope*t0;

        //cout<<"slope = "<<slope<<endl;
        //cout<<"inter = "<<intercept<<endl;
        //cout<<v0<<"  "<<v1<<endl;



        //check variation on all the points between tab and i
        //starting from k = tab+1 to k = i-1
        for(int k = start; k < end; k++){




            double vinterp = slope*data.age[k] + intercept;
            double vreal = a[k];
            //is the difference acceptable?
            double variation = fabs(vinterp - vreal);
            if(variation != 0){
                double error = variation/max(fabs(vinterp),fabs(vreal));
                //cout<<variation<<"   "<<vinterp<<"   "<<vreal<<endl;
                if(error > global::max_rel_error){

                    //the previous point (i-1) must be included in the tables
                    write_tables(end-1);
                    *tab = end-1;
                    //go to the next iteration
                    iteration = true;
                    break;
                }
            }
        }
    
    return iteration;
    
}


bool table::check_variation_pow(double *a, const double v0, const double v1, const double t0, const double inv_t1_minus_t0, const int start, const int end, int *tab){

    bool iteration = false;

    //check mass variation
    const double slope = (v1 - v0)*inv_t1_minus_t0;
    const double intercept = v0 - slope*t0;

    //cout<<"slope = "<<slope<<endl;
    //cout<<"inter = "<<intercept<<endl;
    //cout<<v0<<"  "<<v1<<endl;

    //check variation on all the points between tab and i
    //starting from k = tab+1 to k = i-1
    for(int k = start; k < end; k++){

        double vinterp = slope*data.age[k] + intercept;
        double vreal = pow(10.0, a[k]);
        //is the difference acceptable?
        double variation = fabs(vinterp - vreal);
        if(variation != 0){
            double error = variation/max(fabs(vinterp),fabs(vreal));
            //cout<<variation<<"   "<<vinterp<<"   "<<vreal<<endl;
            if(error > global::max_rel_error){
                //the previous point (i-1) must be included in the tables
                write_tables(end-1);
                *tab = end-1;
                //go to the next iteration
                iteration = true;
                break;
            }
        }
    }

    return iteration;

}

void table::calculate_all_phases() {
  //Parameters for phase change when starting HEcore or COcore
  //double XCEN_tshold_HEcore = 1e-6;
  //double YCEN_tshold_COcore = 1e-6;
  //TSHOLD TO DEFINE STARS DOING THE AGB
  double PSIC_tshold=15;//15; //Tshold based on core degeneration to stop the tracks
  //double CCEN_tshold=0.1; //Tshold based on the Fraction of C in the center
  double MCO_AGB=1.38;//1.38; //Tshold on MCO to trigger ECSN SN
  //Here we assume that if in the last point of the track there is still some consistent fraction of C in the centre
  //and the central degeneration is quite high, the star will enter in the agb phase (see below for the treatment).
  //if (data.PSIC.back()>=PSIC_tshold and data.CCEN.back()>=CCEN_tshold){
  //    cout<<" AGB? YES "<<std::endl;
  //    agb_flag=true;
  //}

  if (global::AGB_cut){

      if(data.mcoreco[maxindex_co_last]< MCO_AGB){
          cout<<" AGB? YES (based on MAX CO)"<<std::endl;
          agb_flag=true;
      }
      else if (data.mcoreco.back() < MCO_AGB and data.mcoreco[maxindex_co_last]>= MCO_AGB){
          cout<<" AGB? YES (based on last point)"<<std::endl;
          agb_flag=true;
      }
      else{
          cout<<" AGB? NO "<<std::endl;
          agb_flag=false;
      }

  }





  bool sethe = false;

    for(size_t i = 0; i < data.age.size(); i++){


        size_t last = data.phase.empty() ? 0: data.phase.size() - 1;


        if (data.qhe[i] != 0.0 && !sethe) { sethe = true;}

        if(data.XCEN[i] > 1e-3 && data.phase.empty()){
            // cout<<PreMainSequence<<endl;
            data.phase.push_back(data.age[i]-data.age[0]);
            data.phase.push_back(PreMainSequence);
            maximum_phase = PreMainSequence;
        }
        //Here we split MainSequence condition between mist and not mist because Lgrav in Mist is in absolute value
        else if(global::stevocode!="mist" &&   data.XCEN[i] > 1e-3 && data.XCEN[i] < data.XCEN[0]*0.99 && data.LGRAV[i] < 0.0 && data.LX[i] > 0.6 && data.phase[last] < MainSequence && data.qhe[i] == 0.0){

            if (data.phase[last]!=PreMainSequence){
                throw errhand_macro("Setting MainSequence but the last phase was not PreMainSequence. File: "+
                global::filename);
            }

            data.phase.push_back(data.age[i]-data.age[0]);
            data.phase.push_back(MainSequence);
            maximum_phase = MainSequence;
        }
        else if(global::stevocode=="mist" &&   data.XCEN[i] > 1e-3 && data.XCEN[i] < data.XCEN[0]*0.99  && data.LX[i] > 0.6 && data.phase[last] < MainSequence && data.qhe[i] == 0.0){

            if (data.phase[last]!=PreMainSequence){
                throw errhand_macro("Setting MainSequence but the last phase was not PreMainSequence. File: "+
                                    global::filename);
            }

            data.phase.push_back(data.age[i]-data.age[0]);
            data.phase.push_back(MainSequence);
            maximum_phase = MainSequence;
        }
        //
        //else if(data.XCEN[i] < 1e-3 && data.XCEN[i] > 1e-8 && data.LY[i] < 0.2 && data.phase[last] < TerminalMainSequence){
        //else if(data.qhe[i] != 0.0 &&  data.qco[i] == 0.0 && data.XCEN[i] < XCEN_tshold_HEcore  && data.phase[last] < TerminalMainSequence){ //the two condtions are mostly equivalent
        else if (data.qhe[i] != 0.0 &&  data.qco[i] == 0.0 && data.phase[last] < TerminalMainSequence){
            if (data.phase[last]!=MainSequence){
                throw errhand_macro("Setting TerminalMainSequence but the last phase was not MainSequence. File: "+
                                    global::filename);
            }

            //cout<<"TerminalMainSequence_H qhe = "<<data.qhe[i]<<"   "<<data.age[i]-data.age[0]<<endl;
            data.phase.push_back(data.age[i-1]-data.age[0]); // take the last point with data.qhe[i] == 0.0
            data.phase.push_back(TerminalMainSequence);
            maximum_phase = TerminalMainSequence;
            //In case we have MHE!=0 before this point reset all the values (HE mass and HE rad) to 0
            reset_hecore(i);
        }

        else if(data.qco[i] == 0.0 && data.XCEN[i] < 1e-8 && data.phase[last] < HshellBurning  ) {

            if (data.phase[last]!=TerminalMainSequence){
                throw errhand_macro("Setting HshellBurning but the last phase was not TerminalMainSequence. File: "+
                                    global::filename);
            }

            data.phase.push_back(data.age[i]-data.age[0]);
            data.phase.push_back(HshellBurning);
            maximum_phase = HshellBurning;
        }
        else if(data.qco[i] == 0.0 && i>maxindex_YCEN_last  && data.YCEN[i]/data.YCEN[maxindex_YCEN_last]<0.99  && data.XCEN[i] < 1e-8 && data.YCEN[i] > 1e-3 && data.phase[last] < HecoreBurning){


            if (data.phase[last]!=HshellBurning){
                throw errhand_macro("Setting HecoreBurning but the last phase was not HshellBurning. File: "+
                                    global::filename);
            }

            data.phase.push_back(data.age[i]-data.age[0]);
            data.phase.push_back(HecoreBurning);
            maximum_phase = HecoreBurning;
        }
        //else if(data.LY[i] > 0.2 && data.XCEN[i] < 1e-8 && data.YCEN[i] > 1e-8 && data.phase[last] < TerminalHecoreBurning){
        //else if(data.qco[i] != 0.0 && data.YCEN[i] < YCEN_tshold_COcore && data.phase[last] < TerminalHecoreBurning){ //the two conditions are mostly equivalent
        else if(data.qco[i] != 0.0 && data.phase[last]<TerminalHecoreBurning){
            if (data.phase[last]!=HecoreBurning){
                throw errhand_macro("Setting TerminalHecoreBurning but the last phase was not HecoreBurning. File: "+
                                    global::filename);
            }

            //cout<<"TerminalCoreHE qCO = "<<" "<<i<<" "<<data.mcoreco[i]<<"   "<<data.age[i]-data.age[0]<<endl;

            data.phase.push_back(data.age[i-1]-data.age[0]); //take the last point with data.qco[i] == 0.0
            data.phase.push_back(TerminalHecoreBurning);
            maximum_phase = TerminalHecoreBurning;
            //In case we have MCO!=0 before this point reset all the values (CO mass and CO rad) to 0
            reset_cocore(i);

        }
        else if(data.qco[i] != 0.0 && data.LC[i] < 0.2 && data.YCEN[i] < 1e-8 && data.phase[last] < HeshellBurning){

            if (data.phase[last]!=TerminalHecoreBurning){
                throw errhand_macro("Setting HeshellBurning but the last phase was not TerminalHecoreBurning. File: "+
                                    global::filename);
            }


            if (i==data.age.size()-1){
                throw errhand_macro("Last phase set exactly at the end of the track, this track will not be written. File: "+
                                    global::filename);
            }


            data.phase.push_back(data.age[i]-data.age[0]);
            data.phase.push_back(HeshellBurning);
            maximum_phase = HeshellBurning;

        }
        //Now check if we have to stop the track before it ends:
        //  Preliminary check:
        //      - Still some fraction (>1e-8) of C in the core
        //      - He totally burnt in the core (YCEN<1e-8)
        //      - Last phase set was HeshellBurning
        //  Specialised stop:
        //      - If the star will go through  the AGB phase (agb_flag>true) and the degeneration is already high
        //      - If the star will not go through the AGB phase che the core Luminosity is already high enough (>0.2)
        //
        else if(data.CCEN[i] > 1e-8 and data.YCEN[i]< 1e-8 and data.phase[last] == HeshellBurning){

            if (!data.PSIC.empty()){
                if (agb_flag and data.PSIC[i]>=PSIC_tshold){
                    std::cout<<" Track stopped before AGB for reaching high degeneration "<<std::endl;
                    maxindex = i;
                    break;
                }
                else if (data.mcoreco[i]>=0.9*data.mcoreco[maxindex_co_first] and data.LC[i] > 0.2){
                    std::cout<<" Track stopped for reaching high LC "<<std::endl;
                    maxindex = i; //we cut all the tracks here!!! this is the beginning of the C-core burning...
                    break;
                }
            }



        }

        /**
        if (data.qhe[i]==0 and data.phase[last]>=TerminalMainSequence){
            data.qhe[i]=data.qhe[i-1];
            data.mcorehe[i]=data.mcorehe[i-1];
            if (!data.rHE.empty())
                data.rHE[i]=data.rHE[i-1];
        }
        **/
        ///SPECIAL treatment for problem in the HE core grow
        //Some time we found (only in MIST tracks so far) that the Helium core start to grow, but then
        //it quickly decrases to 0. This seems to be related to a violent ignition that create a convective zone
        //in the core that it is then cooled by the expansion. THis can happen several times before the HE Core start
        //to grow in a more regular way. We consider these firt phases of HE core growth as transient phases before the
        //real HE core creation. However, sometime we found also that the HE core decreases to 0 in more advanced phase.
        //We decided to handle this in the cose in the following way:
        // 1- If qhe is zero at a certain point in the TerminalMainSequence, we assume that the real HE Core growth has yet to come
        // and we are seeing only a transient  phase, therefore we artificially set to 0 the MHE up to the point where qhe is zero and
        // we reset the phase vector so that we can start to find a new TerminalMainSequence starting point.
        // 2- If we are in the HshellBurning or HecoreNurning phase and we found a point qhe=0, we just assume that qhe remains
        // costant setting qhe[i]=qhe[i-1]. In this way we avoid to come back to a sort of new main sequence phase that is not realistic.
        // In fact, it is true that maybe we have not a convective He core, but still we should have a still strong gradient in the stellar structure separating
        // the more dense inner core from the envelope.
        //In the new SEVN version we assume that there is a difference between WR and naked helium and a star can became NK
        //only after binary process, therefore here we artifically reduce the HE core mass of a small amount. THis is made by the functiion check_wr_star
        if (data.qhe[i]==0 and data.phase[last]==TerminalMainSequence){
            std::cout<<" Restoring TMS set"<<std::endl;
            reset_hecore(i);
            data.phase.erase(data.phase.begin() + 4, data.phase.end());
        }
        else if (data.qhe[i]==0 and data.phase[last]>=HshellBurning){
            data.mcorehe[i] = max(min(data.mass[i]-1E-3,data.mcorehe[i-1]),data.mcoreco[i]);
            //Check if it is smaller than mcoreco
            //data.mcorehe[i] = data.mcorehe[i]<=data.mcoreco[i] ? data.mcoreco[i] + 0.1*(data.mass[i] - data.mcoreco[i]) : data.mcorehe[i] ;
            data.qhe[i]=data.mcorehe[i]/data.mass[i];
            if (!data.rHE.empty()){
                data.rHE[i]= min(0.99*data.r[i],data.rHE[i-1]);
            }

            if(!data.rCO.empty() and data.mcorehe[i]==data.mcoreco[i]){
                data.rHE[i]=data.rCO[i];
            }
            else if(!data.rCO.empty() and data.rHE[i]<=data.rCO[i]){
                data.rHE[i] = data.rCO[i] + 0.1*(data.r[i]-data.rCO[i]);;
            }

        }

        ///SPECIAL treatment for problem in the CO core grow, it is equal to the HE growth see before
        if (data.qco[i]==0 and data.phase[last]==TerminalHecoreBurning){
            std::cout<<" Restoring TCOHEB set"<<std::endl;
            reset_cocore(i);
            data.phase.erase(data.phase.begin() + 10, data.phase.end());
        }
        else if (data.qco[i]==0 and data.phase[last]>=HeshellBurning){
            data.mcoreco[i] = min(data.mcorehe[i],data.mcoreco[i-1]);
            //Check if it is smaller than mcoreco
            data.qco[i]=data.mcoreco[i]/data.mass[i];
            if (!data.rCO.empty() and !data.rHE.empty() and data.mcoreco[i]==data.mcorehe[i]){
                data.rCO[i]= data.rHE[i];
            }
            else if (!data.rCO.empty()){
                data.rCO[i]=data.rCO[i-1];
            }



        }

    }

    if(maximum_phase < HeshellBurning) {
        cout << "This star has not evolved up to the He-shell burning phase. Not writing the track." << endl;
        throw errhand_macro("This star has not evolved up to the He-shell burning phase. Not writing the track. File: "+
                            global::filename);
    }

    if(prev_maximum_phase > maximum_phase) {
        cout << " ******** This track is not complete" << endl <<endl;
        throw errhand_macro("This track is not complete. Not writing the track. File: "+
                            global::filename);
    }

    ofstream ooo;
    ooo.open("prevphase.dat", ios::out);
    ooo<<maximum_phase<<endl;
    ooo.close();

    //cout<<" arrivato alla fine " <<maximum_phase<<endl;

}


///Table pureHE

void table_pureHE::calculate_all_phases() {

    //Parameters for phase change when starting HEcore or COcore
    //double XCEN_tshold_HEcore = 1e-6;
    //double YCEN_tshold_COcore = 1e-6;
    //TSHOLD TO DEFINE STARS DOING THE AGB
    double PSIC_tshold=15;//15; //Tshold based on core degeneration to stop the tracks
    //double CCEN_tshold=0.1; //Tshold based on the Fraction of C in the center
    double MCO_AGB=1.38;//1.38; //Tshold on MCO to trigger ECSN SN
    //Here we assume that if in the last point of the track there is still some consistent fraction of C in the centre
    //and the central degeneration is quite high, the star will enter in the agb phase (see below for the treatment).
    //if (data.PSIC.back()>=PSIC_tshold and data.CCEN.back()>=CCEN_tshold){
    //    cout<<" AGB? YES "<<std::endl;
    //    agb_flag=true;
    //}

    if (global::AGB_cut){

        if(data.mcoreco[maxindex_co_last]< MCO_AGB){
            cout<<" AGB? YES (based on MAX CO)"<<std::endl;
            agb_flag=true;
        }
        else if (data.mcoreco.back() < MCO_AGB and data.mcoreco[maxindex_co_last]>= MCO_AGB){
            cout<<" AGB? YES (based on last point)"<<std::endl;
            agb_flag=true;
        }
        else{
            cout<<" AGB? NO "<<std::endl;
            agb_flag=false;
        }

    }




    if (data.qhe[0]<=0){
        std::cout<<" PureHE with MHE=0 at the beginning. Not writing the track"<<std::endl;
        throw errhand_macro("PureHE with MHE=0 at the beginning. File: "+
                                                  global::filename);
    }


    bool sethe = false;

    for(size_t i = 0; i < data.age.size(); i++){


        size_t last = data.phase.empty() ? 0: data.phase.size() - 1;


        if (data.qhe[i] != 0.0 && !sethe) { sethe = true;}



        //if(data.qco[i] == 0.0 && data.LY[i] > 0.2 && data.XCEN[i] < 1e-8 && data.YCEN[i] > 1e-3 && data.phase.empty()){
        //Let enter always at first step
        if( data.phase.empty()){

            data.phase.push_back(data.age[i]-data.age[0]);
            data.phase.push_back(HecoreBurning);
            maximum_phase = HecoreBurning;
        }
        else if (!data.phase.empty()){
            //else if(data.LY[i] > 0.2 && data.XCEN[i] < 1e-8 && data.YCEN[i] > 1e-8 && data.phase[last] < TerminalHecoreBurning){
            //else if(data.qco[i] != 0.0 && data.YCEN[i] < YCEN_tshold_COcore && data.phase[last] < TerminalHecoreBurning){ //the two conditions are mostly equivalent
            if(data.qco[i] != 0.0 && data.phase[last]<TerminalHecoreBurning){
                if (data.phase[last]!=HecoreBurning){
                    throw errhand_macro("Setting TerminalHecoreBurning but the last phase was not HecoreBurning. File: "+
                                        global::filename);
                }

                //cout<<"TerminalCoreHE qCO = "<<" "<<i<<" "<<data.mcoreco[i]<<"   "<<data.age[i]-data.age[0]<<endl;

                data.phase.push_back(data.age[i-1]-data.age[0]); //take the last point with data.qco[i] == 0.0
                data.phase.push_back(TerminalHecoreBurning);
                maximum_phase = TerminalHecoreBurning;
                //In case we have MCO!=0 before this point reset all the values (CO mass and CO rad) to 0
                reset_cocore(i);

            }
            else if(data.qco[i] != 0.0 && data.LC[i] < 0.2 && data.YCEN[i] < 1e-8 && data.phase[last] < HeshellBurning){



                if (data.phase[last]!=TerminalHecoreBurning){
                    throw errhand_macro("Setting HeshellBurning but the last phase was not TerminalHecoreBurning. File: "+
                                        global::filename);
                }

                if (i==data.age.size()-1){
                    throw errhand_macro("Last phase set exactly at the end of the track, this track will not be written. File: "+
                                        global::filename);
                }



                data.phase.push_back(data.age[i]-data.age[0] );
                data.phase.push_back(HeshellBurning);
                maximum_phase = HeshellBurning;

            }
                //Now check if we have to stop the track before it ends:
                //  Preliminary check:
                //      - Still some fraction (>1e-8) of C in the core
                //      - He totally burnt in the core (YCEN<1e-8)
                //      - Last phase set was HeshellBurning
                //  Specialised stop:
                //      - If the star will go through  the AGB phase (agb_flag>true) and the degeneration is already high
                //      - If the star will not go through the AGB phase che the core Luminosity is already high enough (>0.2)
                //
            else if(data.CCEN[i] > 1e-8 and data.YCEN[i]< 1e-8 and data.phase[last] == HeshellBurning){

                if (!data.PSIC.empty()){
                    if (agb_flag and data.PSIC[i]>=PSIC_tshold){
                        std::cout<<" Track stopped before AGB for reaching high degeneration "<<std::endl;
                        maxindex = i;
                        break;
                    }
                    else if (data.mcoreco[i]>=0.9*data.mcoreco[maxindex_co_first] and data.LC[i] > 0.2){
                        std::cout<<" Track stopped for reaching high LC "<<std::endl;
                        maxindex = i; //we cut all the tracks here!!! this is the beginning of the C-core burning...
                        break;
                    }
                }

            }

            /**
    if (data.qhe[i]==0 and data.phase[last]>=TerminalMainSequence){
        data.qhe[i]=data.qhe[i-1];
        data.mcorehe[i]=data.mcorehe[i-1];
        if (!data.rHE.empty())
            data.rHE[i]=data.rHE[i-1];
    }
    **/
            ///SPECIAL treatment for problem in the CO core grow
            if (data.qco[i]==0 and data.phase[last]==TerminalHecoreBurning){
                std::cout<<" Restoring TCOHEB set"<<std::endl;
                reset_cocore(i);
                data.phase.erase(data.phase.begin() + 10, data.phase.end());
            }
            else if (data.qco[i]==0 and data.phase[last]>=HeshellBurning){
                data.mcoreco[i] = min(data.mcorehe[i],data.mcoreco[i-1]);
                //Check if it is smaller than mcoreco
                data.qco[i]=data.mcoreco[i]/data.mass[i];
                if (!data.rCO.empty() and !data.rHE.empty() and data.mcoreco[i]==data.mcorehe[i]){
                    data.rCO[i]= data.rHE[i];
                }
                else if (!data.rCO.empty()){
                    data.rCO[i]=data.rCO[i-1];
                }
            }




        }


    }


    if(maximum_phase < HeshellBurning) {
        cout << "This star has not evolved up to the He-shell burning phase. Not writing the track." << endl;
        throw errhand_macro("This star has not evolved up to the He-shell burning phase. Not writing the track. File: "+
                            global::filename);
    }

    if(prev_maximum_phase > maximum_phase) {
        cout << " ******** This track is not complete" << endl <<endl;
        throw errhand_macro("This track is not complete. Not writing the track. File: "+
                            global::filename);
    }

    ofstream ooo;
    ooo.open("prevphase.dat", ios::out);
    ooo<<maximum_phase<<endl;
    ooo.close();

}

void table_pureHE::write_tables_last_agb(const int k){

    //NOW special treatment for star stopped by agb (see table::calculate_all_phases).
    //We want the last Mtot is the mass after the AGB phase, we approximate it as Mtot=1.02*MCO
    //In this case we use MHE instead of MCO beacuse this is a pureHE star
    data.mass[k] = std::min(data.mass[k],1.02*data.mcoreco[k]);


    //Check if MHE is larger than the last mass value, in case put MHE=Mass, and RHE=R
    if (data.mcorehe[k]>data.mass[k]){
        data.mcorehe[k]=data.mass[k];
        if (!data.rHE.empty()){
            data.rHE[k]=data.r[k];
        }
    }

    //Inside write_tables MHE=qhe*Mtot and MCO=qco*Mtot, since we fixed mtot by hand we have to change
    //accordingly qhe and qco to preserve MHE and MCO.
    data.qhe[k] = data.mcorehe[k]/data.mass[k]; //MHE/Mtot
    data.qco[k] = data.mcoreco[k]/data.mass[k]; //MCO/Mtot
    write_tables(k);

}
