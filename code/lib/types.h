#ifndef TYPES_H
#define TYPES_H

#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>

using namespace std;


struct Track{

    Track(){

        all_tables.push_back(&age);
        all_tables.push_back(&mass);
        all_tables.push_back(&logl);
        all_tables.push_back(&qhe);
        all_tables.push_back(&qco);
        all_tables.push_back(&XOC);
        all_tables.push_back(&XHS);
        all_tables.push_back(&XHES);
        all_tables.push_back(&XCS);
        all_tables.push_back(&XC13S);
        all_tables.push_back(&QCNV);
        all_tables.push_back(&DPTH_CNV);
        all_tables.push_back(&TCONV);
        all_tables.push_back(&dtime);
        all_tables.push_back(&XNS);
        all_tables.push_back(&XOS);
        all_tables.push_back(&r);
        all_tables.push_back(&qHHE);
        all_tables.push_back(&rHE);
        all_tables.push_back(&rCO);
        all_tables.push_back(&phase);
        all_tables.push_back(&mcorehe);
        all_tables.push_back(&mcoreco);
        all_tables.push_back(&logr);
        all_tables.push_back(&mIn);
        all_tables.push_back(&LGRAV); //L_GRAV
        all_tables.push_back(&XCEN); //XCEN
        all_tables.push_back(&LY); //LY
        all_tables.push_back(&LX);
        all_tables.push_back(&YCEN); //YCEN
        all_tables.push_back(&LC); //LC
        all_tables.push_back(&CCEN); //XC_cen
        all_tables.push_back(&PSIC);


    }

    vector<vector<double>*> all_tables;

    vector<double> age;
    vector<double> mass;
    vector<double> logl;
    vector<double> qhe;
    vector<double> qco;
    vector<double> XOC;
    vector<double> XHS;
    vector<double> XHES;
    vector<double> XCS;
    vector<double> XC13S;
    vector<double> QCNV;
    vector<double> DPTH_CNV;
    vector<double> TCONV;
    vector<double> dtime;
    vector<double> XNS;
    vector<double> XOS;
    vector<double> r;
    vector<double> qHHE;
    vector<double> rHE;
    vector<double> rCO;
    vector<double> phase;
    vector<double> mcorehe;
    vector<double> mcoreco;
    vector<double> logr;
    vector<double> mIn;
    vector<double> LGRAV; //L_GRAV
    vector<double> XCEN; //XCEN
    vector<double> LY; //LY
    vector<double> LX;
    vector<double> YCEN; //YCEN
    vector<double> LC; //LC
    vector<double> CCEN; //XC_cen
    vector<double> PSIC;

};



#endif
