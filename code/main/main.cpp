#include <iostream>

#include <parameters.h>
#include <read_track.h>
#include <table.h>

using namespace std;


int main(int argc, char **argv){
    
    //TYPICAL USAGE: ./track.x -l $(tail ../../tracks/Z0.008Y0.263OUTA1.74_F7_M40.0.PMS | tail -n 3 | head -n 1 | awk '{print $1;}') -f ../../tracks/Z0.008Y0.263OUTA1.74_F7_M40.0.PMS
    
    //FOR FRANEC: ./track.x -l $(wc -l < 040a000.fis) -f ../../tracks/040a000.fis

    std::cout<<"------------- START -------------"<<std::endl;

    try{

    utilis utls;
    bool err;
    
    global::n_lines = 0;
    int prev_in = -1;
  //  double life_in = -1.0;
    
    for(int i = 0; i < argc; i++){
        if(string(argv[i]) == "-l"){
            global::n_lines = utls.string_to_number<int>(string(argv[i+1]), &err);
        }
        if(string(argv[i]) == "-f"){
            global::filename = string(argv[i+1]);
        }
        if(string(argv[i]) == "-prev"){
            prev_in = utls.string_to_number<int>(string(argv[i+1]), &err);;
        }


    }
    
    if(global::n_lines == 0){
        throw errhand_macro("Usage ./tracks -l number_expected_lines. The number of lines cannot be <= 0.");
    }

    if(prev_in == -1)
        throw errhand_macro("Usage ./tracks -prev maximum_phase_previous_track.");

    // if(life_in == -1)
    //    throw errhand_macro("Usage ./tracks -life lifetime_previous_track.");


    parameters param;
    param.read_param();

    // if(global::stevocode == "franec") global::n_lines--; //REMOVE THE HEADER IN THE INE COUNT IN THE CASE OF FRANEC
    // // Remove footer!!
    // if(global::stevocode == "parsec") global::n_lines--;
    // if(global::stevocode == "parsec_old") global::n_lines -= 2;

    readtrack track;
    
    
    
    const string fname = global::filename; 
    track.get_track(fname);
    
    
    table tab(prev_in);
    tab.build();
    
        
    }catch (errhand &t){
        std::cout<<"------------- END -------------"<<std::endl;
        return -1;
    }

    std::cout<<"------------- END -------------"<<std::endl;


    return 0;
    
}
