#ifndef READTRACK_H
#define READTRACK_H

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <cmath>
#include <set>

#include <global.h>
#include <errhand.h>
#include <utilis.h>

using namespace std;

#define errhand_macro(err) errhand(err, __FILE__, __LINE__)

class readtrack{
    
    
public:
    
    readtrack() : n_ignore(global::n_ignore), n_lines(global::n_lines){
        if(!global::calledParams) 
            throw errhand_macro("You need to call class parameters to initialize n_ignore before calling readtrack. File: "+global::filename);

    }
    
    ~readtrack(){}
    
    void get_track(const string fname);
    
    
    
private:
    
    
    
    vector<vector<double>*> pointers_array;
    
    void clean_up();

    std::set<int> find_bad_rows();

    template <typename T>
    void filter_tab_by_id(std::vector<T>* vec, std::set<int> index){

        int offset=0;
        for (auto& j : index){
            vec->erase(vec->begin()+j-offset);
            offset+=1;
        }
    }


    
    const int n_ignore;
    int n_lines;
    
    ifstream in;
    string line;
    bool err;
    utilis utls;
    
    inline bool read_value(istringstream *local_stream, double *outp, int i, int k, const char* file_input, int line_input){

        string loc_value;
        bool read = false;
        local_stream->clear();
        //cout<<" inside read value "<<endl;
        if(*local_stream >> loc_value){

            *outp = utls.string_to_number<double>(loc_value, &err);
            if(err){
                throw errhand("Cannot read a value from the input track, line "+std::to_string(i)+" column "+std::to_string(k)+". File: "+
                        global::filename, file_input, line_input);
            }
            local_stream->clear();
            read = true;
        }

        return read;
    }
    
        
    inline bool read_header(istringstream *local_stream, string *outp, const char* file_input, int line_input){

        string loc_value;
        bool read = false;
        local_stream->clear();

        if(*local_stream >> loc_value){
			  
            *outp = utls.string_to_number<string>(loc_value, &err);
            //if(err)  throw errhand("Cannot read a value from the input track", file_input, line_input);
            local_stream->clear();
            read = true;
        }

        return read;
    }
    
    inline void read_line(ifstream *input_file, istringstream *read_stream){

        string line;

        getline(*input_file, line);
        read_stream->str(line);
        //std::cout<<" Line = "<<line<<endl;

        return;
    }

    void get_pointers_parsec(string string_val){

        // cout<<"["<<string_val<<"]"<<endl;
        //pointers_array[i]->push_back(vvv);

        if (string_val == "AGE"){pointers_array.push_back(&global::data.age); }
        else if (string_val == "Dtime"){pointers_array.push_back(&global::data.dtime);}
        else if (string_val == "MASS"){pointers_array.push_back(&global::data.mass);}
        else if (string_val == "QHEL"){pointers_array.push_back(&global::data.qhe);}
        else if (string_val == "QH_HE"){pointers_array.push_back(&global::data.qHHE);}
        else if (string_val == "QCAROX"){pointers_array.push_back(&global::data.qco);}
        else if (string_val == "Q_CNV"){pointers_array.push_back(&global::data.QCNV);}
        else if (string_val == "DPTH_CNV"){pointers_array.push_back(&global::data.DPTH_CNV);}
        else if (string_val == "TCNV_YR"){pointers_array.push_back(&global::data.TCONV);}

        else if (string_val == "LOG_L"){pointers_array.push_back(&global::data.logl);}
        else if (string_val == "Rstar" || string_val == "RSTAR"){pointers_array.push_back(&global::data.r);}
        else if (string_val == "RHEL"){pointers_array.push_back(&global::data.rHE);}
        else if (string_val == "RCAROX" || string_val == "R_CAROX"){pointers_array.push_back(&global::data.rCO);}
        else if (string_val == "M_INERTI" || string_val == "TOT_INERTIA"){pointers_array.push_back(&global::data.mIn);}

        //else if (string_val == "M_CORE_HE"){pointers_array.push_back(&global::data.mcorehe);cout<<"AGE14"<<endl;}
        //else if (string_val == "LOG_R"){pointers_array.push_back(&global::data.logr);cout<<"AGE15"<<endl;}
        //else if (string_val == "M_CORE_C"){pointers_array.push_back(&global::data.mcoreco);cout<<"AGE16"<<endl;}

        else if (string_val == "L_GRAV"){pointers_array.push_back(&global::data.LGRAV);}
        else if (string_val == "LX"){pointers_array.push_back(&global::data.LX);}
        else if (string_val == "LY"){pointers_array.push_back(&global::data.LY);}
        else if (string_val == "LC"){pointers_array.push_back(&global::data.LC);}

        else if (string_val == "XCEN"){pointers_array.push_back(&global::data.XCEN);}
        else if (string_val == "YCEN"){pointers_array.push_back(&global::data.YCEN);}
        else if (string_val == "XC_cen" || string_val == "XC_CEN"){pointers_array.push_back(&global::data.CCEN);}
        else if (string_val == "XO_cen" || string_val == "O_cen" || string_val == "XO_CEN"){pointers_array.push_back(&global::data.XOC);}
        else if (string_val == "PSI_C"){pointers_array.push_back(&global::data.PSIC);}

        else if (string_val == "Xsup" || string_val == "H_SUP"){pointers_array.push_back(&global::data.XHS);}
        else if (string_val == "Ysup" || string_val == "HE_SUP"){pointers_array.push_back(&global::data.XHES);}
        else if (string_val == "XCsup" || string_val == "C_SUP"){pointers_array.push_back(&global::data.XCS);}
        else if (string_val == "XC13sup"){pointers_array.push_back(&global::data.XC13S);}
        else if (string_val == "XNsup" || string_val == "N_SUP"){pointers_array.push_back(&global::data.XNS);}
        else if (string_val == "XOsup" || string_val == "O_SUP"){pointers_array.push_back(&global::data.XOS);}
        else {
            pointers_array.push_back(nullptr);
            //cout<<"Function/File/Line: "<< __PRETTY_FUNCTION__<<"/"<<__FILE__<<"/"<<__LINE__<<"  unrecognized column" <<endl;
            //cout<<"Column name = "<<string_val<<endl;
        }

        return;
        
    }
    
    void get_pointers_mist(string string_val){

        if (string_val == "star_age"){pointers_array.push_back(&global::data.age); }
        else if (string_val == "star_mass"){pointers_array.push_back(&global::data.mass);}
        else if (string_val == "he_core_mass"){pointers_array.push_back(&global::data.mcorehe);}
        else if (string_val == "c_core_mass"){pointers_array.push_back(&global::data.mcoreco);}
        
        else if (string_val == "log_L"){pointers_array.push_back(&global::data.logl);}
        else if (string_val == "log_R"){pointers_array.push_back(&global::data.logr);}
        
        else if (string_val == "log_abs_Lgrav"){pointers_array.push_back(&global::data.LGRAV);}
        else if (string_val == "log_LH"){pointers_array.push_back(&global::data.LX);}
        else if (string_val == "log_LHe"){pointers_array.push_back(&global::data.LY);}
        else if (string_val == "burn_c"){pointers_array.push_back(&global::data.LC);}

        else if (string_val == "center_h1"){pointers_array.push_back(&global::data.XCEN);}
        else if (string_val == "center_he4"){pointers_array.push_back(&global::data.YCEN);}
        else if (string_val == "center_c12"){pointers_array.push_back(&global::data.CCEN);}
        else if (string_val == "center_o16"){pointers_array.push_back(&global::data.XOC);}
        else if (string_val == "center_degeneracy"){pointers_array.push_back(&global::data.PSIC);}

        else if (string_val == "surface_h1"){pointers_array.push_back(&global::data.XHS);}
        else if (string_val == "surface_he4"){pointers_array.push_back(&global::data.XHES);}
        else if (string_val == "surface_c12"){pointers_array.push_back(&global::data.XCS);}
        else if (string_val == "surface_n14"){pointers_array.push_back(&global::data.XNS);}
        else if (string_val == "surface_o16"){pointers_array.push_back(&global::data.XOS);}
        else {
            pointers_array.push_back(nullptr);
        }

        return;
        
    }
    
    void get_pointers_franec(string string_val, int i){

#if 0
        //pointers_array[i]->push_back(vvv);
        if (string_val == "time"){pointers_array[i] = &(global::data.age);}
        else if (string_val == "M"){pointers_array[i] = &(global::data.mass);}
        else if (string_val == "L"){pointers_array[i] = &(global::data.logl);}
        else if (string_val == "R-Hs"){pointers_array[i] = &(global::data.rHE);}
        else if (string_val == "R-Hes"){pointers_array[i] = &(global::data.rCO);}
		else if (string_val == "M-Hs"){pointers_array[i] = &(global::data.mcorehe);}
		else if (string_val == "R"){pointers_array[i] = &(global::data.logr);}
		else if (string_val == "M-Hes"){pointers_array[i] = &(global::data.mcoreco);}
        else {pointers_array[i] = NULL;}
#endif

        return;
        
    }


    ///COMPLETE TABLES
    inline void complete_table_parsec(int nline){

        if (nline==0){
            global::data.mcorehe.push_back(global::data.qhe[0] * global::data.mass[0]);
            global::data.mcoreco.push_back(global::data.qco[0] * global::data.mass[0]);
            global::data.logr.push_back(log10(global::data.r[0]));
            //For some reason the rHE and rCO in parsec are in log, with 0 that means 0
            //Transform it in lin
            if (!global::data.rHE.empty())
                global::data.rHE[0] = global::data.rHE[0]== 0 ? 0 : std::pow(10.0,global::data.rHE[0]);
            if (!global::data.rCO.empty())
                global::data.rCO[0] = global::data.rCO[0]== 0 ? 0 : std::pow(10.0,global::data.rCO[0]);
        }
        else{
            global::data.mcorehe.push_back(global::data.qhe[nline] * global::data.mass[nline]);
            global::data.mcoreco.push_back(global::data.qco[nline] * global::data.mass[nline]);
            global::data.logr.push_back(log10(global::data.r[nline]));
            //For some reason the rHE and rCO in parsec are in log, with 0 that means 0
            //Transform it in lin
            if (!global::data.rHE.empty())
                global::data.rHE[nline] = global::data.rHE[nline]== 0 ? 0 : std::pow(10.0,global::data.rHE[nline]);
            if (!global::data.rCO.empty())
                global::data.rCO[nline] = global::data.rCO[nline]== 0 ? 0 : std::pow(10.0,global::data.rCO[nline]);
        }

        return;
    }

    //TODO We have cretaed correction for qhe=0 and qhe=1 here and there, but I think is safer to correct everything possibile here as done now
    inline void complete_table_mist(int nline){

        //global::data.age[nline] = global::data.age[nline] - global::data.age[0];
        //Some time in MIst we see tracks with MHE=0 when a MCO is already present (we have not radii in mist so this is not a problem)
        if (global::data.mcorehe[nline]<global::data.mcoreco[nline] and global::data.mcoreco[nline]!=0.){
            global::data.mcorehe[nline] = global::data.mcoreco[nline];
        }
        global::data.qhe.push_back(global::data.mcorehe[nline] / global::data.mass[nline]);
        global::data.qco.push_back(global::data.mcoreco[nline] / global::data.mass[nline]);
        global::data.r.push_back(pow(10,global::data.logr[nline]));


        //In case of mist track, compute the fraction luminosity
        global::data.LX[nline] = pow(10,global::data.LX[nline])/pow(10,global::data.logl[nline]);
        global::data.LY[nline] = pow(10,global::data.LY[nline])/pow(10,global::data.logl[nline]);
        global::data.LC[nline] = pow(10,global::data.LC[nline])/pow(10,global::data.logl[nline]);
        global::data.LGRAV[nline] = pow(10,global::data.LGRAV[nline])/pow(10,global::data.logl[nline]);



        return;
    }


    inline void complete_table_franec(int nline){

        //global::data.age[nline] = global::data.age[nline] - global::data.age[0];
        global::data.qhe.push_back(global::data.mcorehe[nline] / global::data.mass[nline]);
        global::data.qco.push_back(global::data.mcoreco[nline] / global::data.mass[nline]);
        global::data.r.push_back(pow(10,global::data.logr[nline]));

        return;
    }

    
    
};



#endif
