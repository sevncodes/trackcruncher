for DIR in tracks/* ; do

	echo "DIRECTORY = $DIR"
	v2=${DIR:7} #remove the first N characters  in the string of the directory
	v2=${v2//.} #remove the dot character from the metallicity, if any
	v2=${v2//\/} #remove the slash character of the directory, if any
	DIROUT=${v2//Z} #remove the Z character of the metallicity, if any
	echo "OUTPUT IN DIR = $DIROUT"

	rm -rf ./output/$DIROUT
	mkdir -p ./output/$DIROUT

	rm -rf mco.dat phase.dat mhe.dat lumi.dat  mass.dat  radius.dat  time.dat prevphase.dat lifetime.dat hsup.dat hesup.dat csup.dat osup.dat nsup.dat
	touch mco.dat phase.dat mhe.dat lumi.dat  mass.dat  radius.dat  time.dat hsup.dat hesup.dat csup.dat osup.dat nsup.dat

	#the following line counts the number of track files in the folder
	NUMFILES=$(ls -l $DIR | grep -v ^l | wc -l)
  echo "$NUMFILES"
	NUMFILES=`expr $NUMFILES - 1`

	#cycle on the "n" head parameter to extract info from all the tracks in the fodler
	for i in `seq 1 $NUMFILES`; do

		if [ ! -f prevphase.dat ];
		then
			PREVPHASE=0
		else
			PREVPHASE=$(<prevphase.dat)
		fi

		echo "PREVPHASE = $PREVPHASE"

    FILENAME=$(find $DIR -maxdepth 1 -type f  -exec basename {} \; | LC_ALL=C sort -g | head -n $i | tail -n 1) #take only .sevn files
  	echo "FILENAME $FILENAME"
  	EFFECTIVELINES=`expr $(wc -l $DIR/$FILENAME | awk '{print $1;}') - 2`
  	./track.x -l $EFFECTIVELINES -f $DIR/$FILENAME -prev $PREVPHASE
    echo "COMMAND: ./track.x -l $EFFECTIVELINES -f $DIR/$FILENAME -prev $PREVPHASE"


		cat mco.dat mco_line.dat > COMatrixtmp.dat
		mv COMatrixtmp.dat mco.dat

		cat mhe.dat mhe_line.dat > HEMatrixtmp.dat
		mv HEMatrixtmp.dat mhe.dat

		cat lumi.dat lumi_line.dat > LuminosityMatrixtmp.dat
		mv LuminosityMatrixtmp.dat lumi.dat

		cat mass.dat mass_line.dat > MassMatrixtmp.dat
		mv MassMatrixtmp.dat mass.dat

		cat radius.dat radius_line.dat > RadiusMatrixtmp.dat
		mv RadiusMatrixtmp.dat radius.dat

		cat time.dat time_line.dat > TimeMatrixtmp.dat
		mv TimeMatrixtmp.dat time.dat

		cat phase.dat phase_line.dat > TypeMatrixtmp.dat
		mv TypeMatrixtmp.dat phase.dat

		cat hsup.dat hsup_line.dat > HsupMatrixtmp.dat
		mv HsupMatrixtmp.dat hsup.dat

		cat hesup.dat hesup_line.dat > HEsupMatrixtmp.dat
		mv HEsupMatrixtmp.dat hesup.dat

		cat osup.dat osup_line.dat > OsupMatrixtmp.dat
		mv OsupMatrixtmp.dat osup.dat

		cat csup.dat csup_line.dat > CsupMatrixtmp.dat
		mv CsupMatrixtmp.dat csup.dat

		cat nsup.dat nsup_line.dat > NsupMatrixtmp.dat
		mv NsupMatrixtmp.dat nsup.dat

		rm -rf time_line.dat radius_line.dat mass_line.dat lumi_line.dat mhe_line.dat mco_line.dat phase_line.dat hsup_line.dat hesup_line.dat csup_line.dat osup_line.dat nsup_line.dat

	done

	mv mco.dat phase.dat mhe.dat lumi.dat  mass.dat  radius.dat  time.dat  hsup.dat hesup.dat nsup.dat osup.dat csup.dat ./output/$DIROUT
	echo "mv mco.dat phase.dat mhe.dat lumi.dat  mass.dat  radius.dat  time.dat  hsup.dat hesup.dat nsup.dat osup.dat csup.dat ./output/$DIROUT"

done
