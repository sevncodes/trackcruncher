#ifndef GLOBAL_H
#define GLOBAL_H

#include <iostream>

#include <types.h>

using namespace std;

class global{
    
    
public:
    global(){}
    ~global(){}
    
    static int n_ignore;
    static int n_lines;
    static double max_rel_error;
    static int ndigit;
    static string filename;
    static string stevocode;
    static bool AGB_cut;
    
    static Track data;
    
    static bool calledParams;
    static bool calledTrack;
    
    
private:
    
    
};


#endif
