DIR=/home/kirilanshelo/Scrivania/Uni/Tesi\ Magistrale/sevn/franec/codeinterp/tracks/marco
DIROUT=/home/kirilanshelo/Scrivania/Uni/Tesi\ Magistrale/sevn/franec/codeinterp/matrices/marco_002

mkdir -p $DIROUT
rm -rf $DIROUT/*


rm -rf mco.dat mhe.dat lumi.dat  mass.dat  radius.dat  time.dat  rhe.dat rco.dat  phase.dat prevphase.dat lifetime.dat
touch mco.dat mhe.dat lumi.dat  mass.dat  radius.dat  time.dat  rhe.dat rco.dat phase.dat

#the following line counts the number of track files in the folder
NUMFILES=$(ls -l $DIR | grep -v ^l | wc -l)
NUMFILES=`expr $NUMFILES - 1`


#cycle on the "n" head parameter to extract info from all the tracks in the fodler
for i in `seq 1 $NUMFILES`; do
	FILENAME=$(find $DIR -maxdepth 1 -type f -printf "%f\n" | sort -V | head -n $i | tail -n 1)
	echo "$FILENAME"

	./track.x -l $(wc -l < $DIR/$FILENAME) -f $DIR/$FILENAME

	cat mco.dat mco_line.dat > COMatrixtmp.dat
	mv COMatrixtmp.dat mco.dat

	cat mhe.dat mhe_line.dat > HEMatrixtmp.dat
	mv HEMatrixtmp.dat mhe.dat

	cat lumi.dat lumi_line.dat > LuminosityMatrixtmp.dat
	mv LuminosityMatrixtmp.dat lumi.dat

	cat mass.dat mass_line.dat > MassMatrixtmp.dat
	mv MassMatrixtmp.dat mass.dat

	cat radius.dat radius_line.dat > RadiusMatrixtmp.dat
	mv RadiusMatrixtmp.dat RadiusMatrix.dat

	cat time.dat time_line.dat > TimeMatrixtmp.dat
	mv TimeMatrixtmp.dat time.dat

	cat rhe.dat rhe_line.dat > RHEMatrixtmp.dat
	mv RHEMatrixtmp.dat rhe.dat

	cat rco.dat rco_line.dat > RCOMatrixtmp.dat
	mv RCOMatrixtmp.dat rco.dat

	cat phase.dat phase_line.dat > TypeMatrixtmp.dat
	mv TypeMatrixtmp.dat phase.dat

	rm -rf time_line.dat radius_line.dat mass_line.dat lumi_line.dat mhe_line.dat mco_line.dat phase_line.dat


done

mv mco.dat phase.dat mhe.dat lumi.dat  mass.dat  radius.dat rhe.dat rco.dat time.dat   ./output/$DIROUT
echo "mv mco.dat phase.dat mhe.dat lumi.dat  mass.dat  radius.dat  time.dat  rhe.dat rco.dat ./output/$DIROUT"
