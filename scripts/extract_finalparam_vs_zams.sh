awk '{print $NF}' mco.dat > cofin.dat
awk '{print $NF}' mhe.dat > hefin.dat
awk '{print $NF}' mass.dat > mfin.dat
awk '{print $1}' mass.dat > zams.dat

paste zams.dat mfin.dat hefin.dat cofin.dat > Final_vs_zams.dat

rm -rf cofin.dat hefin.dat mfin.dat zams.dat

