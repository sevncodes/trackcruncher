#ifndef TABLEH
#define TABLEH

#include <iostream>
#include <cmath>

#include <global.h>
#include <types.h>
#include <errhand.h>
#include <algorithm>
#include <utilis.h>


using namespace std;

#define errhand_macro(err) errhand(err, __FILE__, __LINE__)


class table{
    
    
public:
    
    std::vector<std::string> outfile_list_parsec;

    std::vector<std::string> outfile_list_franec;

    std::vector<std::string> outfile_list_mist;


    table(int prev_in){
        outfile_list_parsec = {"mass_line.dat",
                                                    "phase_line.dat",
                                                    "mco_line.dat",
                                                    "tconv_line.dat",
                                                    "mhe_line.dat",
                                                    "radius_line.dat",
                                                    "qconv_line.dat",
                                                    "depthconv_line.dat",
                                                    "time_line.dat",
                                                    "inertia_line.dat",
                                                    "lumi_line.dat",
                                                    "rhe_line.dat",
                                                    "rco_line.dat",
                                                    "hsup_line.dat",
                                                    "hesup_line.dat",
                                                    "csup_line.dat",
                                                    "osup_line.dat",
                                                    "nsup_line.dat"};

        outfile_list_franec = {"mass_line.dat",
                                                    "phase_line.dat",
                                                    "mco_line.dat",
                                                    "tconv_line.dat",
                                                    "mhe_line.dat",
                                                    "radius_line.dat",
                                                    "qconv_line.dat",
                                                    "depthconv_line.dat",
                                                    "time_line.dat",
                                                    "inertia_line.dat",
                                                    "lumi_line.dat",
                                                    "rhe_line.dat",
                                                    "rco_line.dat",
                                                    "hsup_line.dat",
                                                    "hesup_line.dat",
                                                    "csup_line.dat",
                                                    "osup_line.dat",
                                                    "nsup_line.dat"};
        outfile_list_mist = {"mass_line.dat",
                                                  "phase_line.dat",
                                                  "mco_line.dat",
                                                  "mhe_line.dat",
                                                  "radius_line.dat",
                                                  "time_line.dat",
                                                  "lumi_line.dat",
                                                  "hsup_line.dat",
                                                  "hesup_line.dat",
                                                  "csup_line.dat",
                                                  "osup_line.dat",
                                                  "nsup_line.dat"};

        if(!global::calledTrack)
            throw errhand_macro("You need to call class read_track to initialize global::data before calling the table class");
        
        data = global::data;
        elements = 0;
        prev_maximum_phase = prev_in;
        //prev_life = life_in;
        
        std::vector<std::string> outfiles;
        ofstream out;

        if(global::stevocode == "parsec" || global::stevocode == "parsec_old"){
            outfiles = outfile_list_parsec;
        }
        else if (global::stevocode == "franec") {outfiles = outfile_list_franec;}
        else if (global::stevocode == "mist") {outfiles = outfile_list_mist;}

        for(int i = 0; i < outfiles.size(); i++){
            out.open(outfiles[i], ios::out);
            out.close();
        }

    }
    
    ~table(){}
    
    virtual void build();


protected:
    //this line must be taken as input in the parameter file!!!!
    enum Phases {
        PreMainSequence = 0,
        MainSequence,
        TerminalMainSequence,
        HshellBurning,
        HecoreBurning,
        TerminalHecoreBurning,
        HeshellBurning,
        Remnant,
        Nphases
    };

    Track data;
    int elements;
    int maximum_phase, prev_maximum_phase;
    double prev_life;
    size_t maxindex;
    bool agb_flag=false;
    unsigned int maxindex_co_first;
    unsigned int maxindex_co_last;
    unsigned int maxindex_YCEN_first;
    unsigned int maxindex_YCEN_last;
    utilis utls;

    virtual void calculate_all_phases();
    virtual void write_tables(const int k);
    virtual void write_tables_last_agb(const int k);
    /**
     * Utility to write the last point in the table.
     * In this case, we don't check for wr (MHE can be equal to Mass).
     * The last point can be written using write_tables_last_agb, where we modify the final total mass by hand
     * or using the normal write_table. There is also a check on the times, if the time difference is smaller than
     * the output resolution insert use the smallest possible resolution as dt
     * @param tab Last index written
     * @param last_point Index of the last point, if is negative, use the actual last point in the tracks
     */
    virtual inline void write_last(int tab, int last_point=-1){

        last_point = last_point<=0 ? data.mass.size() - 1 : last_point;

        std::cout<<last_point<<std::endl;

        if (last_point<=tab)
            throw errhand_macro("Trying to write the last point, but the last index (" + std::to_string(last_point) +
            ") is smaller than the last point recorded from the tracks (" + std::to_string(tab) +"). File "+global::filename);


        double MCO_tshold=0.01;
        bool is_MCO_lower_than_max = (data.mcoreco[last_point] - data.mcoreco[maxindex_co_last])/data.mcoreco[maxindex_co_last]<-MCO_tshold;

        //If tab has not reached maxindex_co_last and the MCO in the last point is smaller than the maximum, set the
        //last point to the index where MCO is the maximum
        if (tab<maxindex_co_last and is_MCO_lower_than_max){
            last_point=maxindex_co_last;
            std::cout<<" Re-setting last point at index at maxMCO i="<<last_point<<std::endl;
        }
        //If tab has already reached maxindex_co and the last point has a MCO smaller than the maximum
        //just stop at the next point on the table. In principle we could stop just at tab, but if
        //we have to make the agb_flag modification, we have to put a last point on the table (otherwise we
        //to rewrite the last one, myabe it could be done in a future update).
        else if ( is_MCO_lower_than_max and (tab<data.mass.size() - 1) ){
            last_point=tab+1;
            std::cout<<" Re-setting last point at index tab+1 i="<<last_point<<std::endl;
        }

        //Check if last point has an age larger that the last phase
        double Min_time=data.phase[data.phase.size()-2]; //We cannot assign last point with time smaller than this

        while ( (data.age[last_point]-data.age[0])<=Min_time ){
            //std::cout<<" E "<<last_point<<" "<<data.age[last_point]-data.age[0]<<" "<<Min_time<<std::endl;
            last_point+=1;

            if (last_point==data.age.size()-1)
                throw errhand_macro("Trying to write the last point, but the last point on the track for which t[-1] is equal or smaller"
                                    "than the time of the last phase. File "+global::filename);


        }


        std::cout<<std::setprecision(15)<<" - Last point info: Index="<<last_point<<", MCO="<<data.mcoreco[last_point]<<", Time="<<data.age[last_point]-data.age[0]<<std::endl;
        std::cout<<std::setprecision(15)<<" - Penultimate point info: Index="<<tab<<", MCO="<<data.mcoreco[tab]<<", Time="<<data.age[tab]-data.age[0]<<std::endl;
        std::cout<<std::setprecision(15)<<" - Last point on tracks: Index="<<data.mass.size() - 1<<", MCO="<<data.mcoreco[data.mass.size() - 1]<<", Time="<<data.age[data.mass.size() - 1]-data.age[0]<<std::endl;
        std::cout<<std::setprecision(15)<<" - Maximum CO info:  MaxCO="<< data.mcoreco[maxindex_co_last] <<", Index_first="<< maxindex_co_first <<", Time_first="<< data.age[maxindex_co_first]-data.age[0]
        <<", Index_last="<< maxindex_co_last <<" Time_last="<< data.age[maxindex_co_last]-data.age[0] <<std::endl;
        std::cout<<std::setprecision(15)<<" - Last Phase info: Time="<<Min_time<<std::endl;

        //Print the last point in any case, with a special treatment for intermediate mass star in the GB
        double orderofmag = (int)log10(data.age[last_point]);
        //if the time resolution is very small just use the smallest possible resolution in dt for the last point
        if(data.age[last_point] - data.age[tab] <= pow(10,-global::ndigit)*pow(10.0, orderofmag)) data.age[last_point] = data.age[tab] + pow(10,-global::ndigit)*pow(10.0, orderofmag);

        //NOW special treatment for star stopped by agb (see table::calculate_all_phases).
        //We want the last Mtot is the mass after the AGB phase, we approximate it as Mtot=1.02*MHE
        if (agb_flag){
            write_tables_last_agb(last_point);
        } else {
            //check_wr_star(tab);
            write_tables(last_point); //LAST point always in tables
        }
    }

    virtual bool check_variation(double *a,
                         const double v0, const double v1, const double t0, const double inv_t1_minus_t0, 
                         const int start, const int end, 
                         int *tab);
    virtual bool check_variation_pow(double *a,
                         const double v0, const double v1, const double t0, const double inv_t1_minus_t0,
                         const int start, const int end,
                         int *tab);

    virtual void  write_phases(){

        ofstream out;
        out.open("phase_line.dat", ios::out);


        out << scientific << setprecision(global::ndigit)<<data.phase[0]/1e6 << "   " << fixed << setprecision(1) << (int)data.phase[1]<<"   ";
        int writtenphase = (int)data.phase[1];

        for(size_t i = 2; i < data.phase.size(); i+=2) {
            if(data.phase[i+1] != writtenphase) {
                out << scientific << setprecision(global::ndigit) <<data.phase[i]/1e6 << "   " << fixed << setprecision(1) << (int)data.phase[i + 1] << "   ";
                writtenphase = (int)data.phase[i + 1];
            }
            else
                continue;
        }

        out.close();

    }

    virtual void write_endl(){
     
        ofstream out;
        std::vector<std::string> outfiles;

        if(global::stevocode == "parsec" || global::stevocode == "parsec_old"){
            outfiles = outfile_list_parsec;
        }
        else if (global::stevocode == "franec") {outfiles = outfile_list_franec;}
        else if (global::stevocode == "mist") {outfiles = outfile_list_mist;}

        for(int i = 0; i < outfiles.size(); i++){
            out.open(outfiles[i], ios::app);
            out<<scientific<<setprecision(global::ndigit);
            out<<endl;
            out.close();
        }

        return;
        
    }

    virtual inline bool check_if_co_decreases(int i, int tab, double tshold=0.01){

        //First check:
        // - if we have co!=0
        // - if i is not 0
        // - if we have still to reach the maximum_co in tracks
        if (data.qco[i]==0 or data.qco[tab]==0 or i==0 or i<=maxindex_co_first)
            return false;

        //Check if CO is decreasing within a given tshold
        //if ((data.mcoreco[i]-data.mcoreco[tab])/data.mcoreco[tab]>-tshold)
        //     return false;

        bool check_one_step = (data.mcoreco[i]-data.mcoreco[i-1])/data.mcoreco[i-1]>-tshold;
        bool check_tab_step = (data.mcoreco[i]-data.mcoreco[tab])/data.mcoreco[tab]>-tshold;

        if (check_one_step and check_tab_step)
             return false;


         //If we are here we can decide if it is needed to stop
        //Condition to enter in this check (it was already here for parsec and parsec_old), now we are using htis for all
        //GI, Here we added also the condition that we can stop only if the maximum CO has been already reached (i>=maxindex_co_first)
        if(data.qco[i] < 0.972 and data.qco[i] > 0.05){


            int loc_tab = i-1; //write the previous point in tables... which is the last point
            cout<<" found decreasing CO (i="<<i<<", last="<<tab<<")... stop writing the track "<<endl;
            cout<<" MCO[last]="<<std::setprecision(10)<<data.mcoreco[tab]<<" MCO[i]="<<data.mcoreco[i]<<" MCO[i-1]="<<data.mcoreco[i-1]<<std::endl;
            cout<<" MHE[last]="<<std::setprecision(10)<<data.mcorehe[tab]<<" MHE[i]="<<data.mcorehe[i]<<" MHE[i-1]="<<data.mcorehe[i-1]<<std::endl;



            double Min_time=data.phase[data.phase.size()-2]; //We cannot assign last point with time smaller than this

            //If loc_table larger than table write it
            if (loc_tab>tab){
                write_last(tab,loc_tab); //Handling of the min time is inside write_last, so it is not sure we will write the end at loc_table
            }
            else if( (data.age[tab]-data.age[0])<=Min_time){
                //We cannot stop at tab so write the next step possible
                write_last(tab,tab+1); //Handling of the min time is inside write_last, so it is not sure we will write the end at tab+1
            }

            write_endl();


            return true;


        }

        /*
        if(global::stevocode == "parsec" || global::stevocode == "parsec_old"){
        
            if(data.qco[i] < 0.972 && data.qco[i] > 0.05 && data.qco[tab] != 0.0){
              if ( (data.qco[i]*data.mass[i] - data.qco[tab]*data.mass[tab])/(data.qco[tab]*data.mass[tab]) < -0.01){
                  int loc_tab = i-1; //write the previous point in tables... which is the last point
                  write_tables(loc_tab);
                  cout<<" i = "<<i<<endl;
                  cout<<" found decreasing CO... stop writing the track "<<endl;
                  cout<<data.qco[i]*data.mass[i]<<"   "<<data.qco[tab]*data.mass[tab]<<"   "<<data.qco[i]<<endl;
                  write_endl();
                  return true;
		    }
		  }
		  
        }
        
        if(global::stevocode == "franec"){
        
            if(data.mcoreco[i] == 0.0 && data.mcoreco[tab] != 0.0){ 
                // bug of the franec code?? CO goes to 0 after being != 0.0
                
                for(int k = 0; k <= tab; k++)
                    data.mcoreco[k] = 0.0;
            }
            
            if(data.mcoreco[i] < data.mcoreco[i-1] && data.mcoreco[i] != 0.0){
                data.mcoreco[i] = data.mcoreco[i-1];
            }

                
        }
        */
            
        return false;
            
    }

    //TODO This check was made to handle the mcorehe going to 0 in Franec, but now we added a more general handling inside calculate_all_phases, we should use a unique way to handle this
    virtual void check_wr_star(int i){
     
        if(global::stevocode == "franec"){
         
            if(data.mcorehe[i] == 0.0 && data.mcorehe[i-1] != 0.0){
                //the star has become a WR... so
                data.mcorehe[i] = min(data.mass[i], data.mcorehe[i-1]);
                data.qhe[i]=data.mcorehe[i]/data.mass[i];
                //so it stays constant until it becomes equal to the total mass of the star
                data.rHE[i] = pow(10, data.logr[i]); //AGGIUNTA EM
            }
           
        }

        //In Mist we found that it can happens that MHE==Mass, i.e. the star is a WR.
        //However is SEVN we separate WR (that should have a tiny H envelope) and pureHE (that can be created only through binary evolution)
        //Therefore when MHE=MAss we remove a tinu amount from MHE.
        //In Mist we don't have radii for the core so we have to modify just the masses
        if(global::stevocode == "mist"){

            if (data.mcorehe[i]>=data.mass[i]){
                data.mcorehe[i] = 0.999*data.mass[i];
                data.mcorehe[i] = data.mcorehe[i]<=data.mcoreco[i] ?
                                  data.mcoreco[i] + 0.999 * (data.mass[i]-data.mcoreco[i])  :
                                  data.mcorehe[i];
            }
            data.qhe[i] = data.mcorehe[i]/data.mass[i];

        }
        
        if(global::stevocode == "parsec" || global::stevocode == "parsec_old"){


            //if(data.qHHE[i] == 0.0 && data.qhe[i] != 0.0){
                //data.qhe[i] = 1.0;
                //candecrease = true; //helium can decrease only in this case
                //std::cout<<"WR mass after = "<<data.qhe[i]*data.mass[i]<<"    "<<data.mass[i]<<endl;
            //}

#if 0
            if(data.qhe[i]*data.mass[i] < data.qhe[i-1]*data.mass[i-1]){

                std::cout<<"WR mass before = "<<data.qhe[i-1]*data.mass[i-1]<<"    "<<data.qhe[i]*data.mass[i]<<endl;
                //the star has become a WR... so
                data.qhe[i] = min(data.qhe[i-1]*data.mass[i-1]/data.mass[i], 1.0);
                //so it stays constant until it becomes equal to the total mass of the star

                std::cout<<"WR mass after = "<<data.qhe[i-1]*data.mass[i-1]<<"    "<<data.qhe[i]*data.mass[i]<<endl;
                //exit(1);
                
            }
#endif

        }
        
        
        return;
        
    }

    /**
     * Reset tables values related to HE core up to index maxi (excluded)
     * @param maxi
     */
    inline void reset_hecore(int maxi){
        for (int i=0; i<maxi; i++){
            data.qhe[i] = 0.0;
            data.mcorehe[i] = 0.0;
            if (!data.rHE.empty())
                data.rHE[i] = 0.0;
        }
    }
    /**
     *
     * Reset tables values related to CO core up to index maxi (excluded)
     * @param maxi
     */
    inline void reset_cocore(int maxi){
        for (int i=0; i<maxi; i++){
            data.qco[i] = 0.0;
            data.mcoreco[i] = 0.0;
            if (!data.rCO.empty())
                data.rCO[i] = 0.0;
        }
        set_maxco_index();
    }

    virtual inline void set_maxco_index(){
        maxindex_co_first = std::max_element(data.mcoreco.begin(),data.mcoreco.end()) -  data.mcoreco.begin();
        maxindex_co_last  = (data.mcoreco.size()-1) - (std::max_element(data.mcoreco.rbegin(),data.mcoreco.rend()) -  data.mcoreco.rbegin());
    }

    virtual inline void set_maxYCEN_index(){
        maxindex_YCEN_first = std::max_element(data.YCEN.begin(),data.YCEN.end()) -  data.YCEN.begin();
        maxindex_YCEN_last =  (data.YCEN.size()-1) - (std::max_element(data.YCEN.rbegin(),data.YCEN.rend()) -  data.YCEN.rbegin());

    }

    ///Caclulate phases
    //calculate_phases_parsec(int nline);

};


class table_pureHE : public table{

public:
    table_pureHE(int prev_in) : table(prev_in){} //Just call parent constructor

    ~table_pureHE(){}

protected:

    void calculate_all_phases() override ;

    void write_tables_last_agb(const int k) override ;


};

class table_parsec : public table{

};

class table_mist : public table{

};

class table_franec : public table{

};

#endif
