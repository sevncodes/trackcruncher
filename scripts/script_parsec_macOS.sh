for DIR in tracks/*/ ; do

	echo "DIRECTORY = $DIR"
	v2=${DIR:7} #remove the first N characters  in the string of the directory
	v2=${v2//.} #remove the dot character from the metallicity, if any
	v2=${v2//\/} #remove the slash character of the directory, if any
	DIROUT=${v2//Z} #remove the Z character of the metallicity, if any
	echo "OUTPUT IN DIR = $DIROUT"

rm -rf ./output/$DIROUT
mkdir -p ./output/$DIROUT

rm -rf mco.dat phase.dat mhe.dat lumi.dat  mass.dat  radius.dat  time.dat  rhe.dat rco.dat inertia.dat prevphase.dat lifetime.dat hsup.dat hesup.dat csup.dat osup.dat nsup.dat qconv.dat depthconv.dat tconv.dat
touch mco.dat phase.dat mhe.dat lumi.dat  mass.dat  radius.dat  time.dat  rhe.dat rco.dat inertia.dat hsup.dat hesup.dat csup.dat osup.dat nsup.dat qconv.dat depthconv.dat tconv.dat


#prepare to sort files according to the star's mass
rm -rf $DIR/*.sevn

for i in "$DIR"/*F71*.TAB ; do #only F71 and .TAB files
	var=${i#*M}
	var=${var//.TAB}

	result=$(LC_NUMERIC="en_US.UTF-8" printf "%e\n" $var)

	cp $i "$DIR"/"$result.sevn"
done


#the following line counts the number of track files in the folder
NUMFILES=$(ls -l $DIR/*.sevn  | grep -v ^l | wc -l)
NUMFILES=`expr $NUMFILES`

#cycle on the "n" head parameter to extract info from all the tracks in the fodler
for i in `seq 1 $NUMFILES`; do

	if [ ! -f prevphase.dat ];
	then
		PREVPHASE=0
	else
		PREVPHASE=$(<prevphase.dat)
	fi

	echo "PREVPHASE = $PREVPHASE"

	FILENAME=$(find $DIR -maxdepth 1 -type f -name "*.sevn"  -exec basename {} \; | LC_ALL=C sort -g | head -n $i | tail -n 1) #take only .sevn files
	echo "FILENAME $FILENAME"
	EFFECTIVELINES=`expr $(wc -l $DIR/$FILENAME | awk '{print $1;}') - 2`
	./track.x -l $EFFECTIVELINES -f $DIR/$FILENAME -prev $PREVPHASE
  echo "-l $EFFECTIVELINES -f $DIR/$FILENAME -prev $PREVPHASE"

	cat mco.dat mco_line.dat > COMatrixtmp.dat
	mv COMatrixtmp.dat mco.dat

	cat mhe.dat mhe_line.dat > HEMatrixtmp.dat
	mv HEMatrixtmp.dat mhe.dat

	cat lumi.dat lumi_line.dat > LuminosityMatrixtmp.dat
	mv LuminosityMatrixtmp.dat lumi.dat

	cat mass.dat mass_line.dat > MassMatrixtmp.dat
	mv MassMatrixtmp.dat mass.dat

	cat radius.dat radius_line.dat > RadiusMatrixtmp.dat
	mv RadiusMatrixtmp.dat radius.dat

	cat time.dat time_line.dat > TimeMatrixtmp.dat
	mv TimeMatrixtmp.dat time.dat

	cat phase.dat phase_line.dat > TypeMatrixtmp.dat
	mv TypeMatrixtmp.dat phase.dat

	cat rhe.dat rHE_line.dat > RHEMatrixtmp.dat
	mv RHEMatrixtmp.dat rhe.dat

	cat rco.dat rCO_line.dat > RCOMatrixtmp.dat
	mv RCOMatrixtmp.dat rco.dat

	cat inertia.dat inertia_line.dat > InertiaMatrixtmp.dat
	mv InertiaMatrixtmp.dat inertia.dat

	cat hsup.dat hsup_line.dat > HsupMatrixtmp.dat
	mv HsupMatrixtmp.dat hsup.dat

	cat hesup.dat hesup_line.dat > HEsupMatrixtmp.dat
  mv HEsupMatrixtmp.dat hesup.dat

	cat osup.dat osup_line.dat > OsupMatrixtmp.dat
  mv OsupMatrixtmp.dat osup.dat

	cat csup.dat csup_line.dat > CsupMatrixtmp.dat
  mv CsupMatrixtmp.dat csup.dat

	cat nsup.dat nsup_line.dat > NsupMatrixtmp.dat
  mv NsupMatrixtmp.dat nsup.dat

	cat qconv.dat qconv_line.dat > QCONVMatrixtmp.dat
  mv QCONVMatrixtmp.dat qconv.dat

	cat depthconv.dat depthconv_line.dat > DPTCNVMatrixtmp.dat
  mv DPTCNVMatrixtmp.dat depthconv.dat

	cat tconv.dat tconv_line.dat > TCONVMatrixtmp.dat
  mv TCONVMatrixtmp.dat tconv.dat

	rm -rf inertia_line.dat rco_line.dat rhe_line.dat time_line.dat radius_line.dat mass_line.dat lumi_line.dat mhe_line.dat mco_line.dat phase_line.dat hsup_line.dat hesup_line.dat csup_line.dat osup_line.dat nsup_line.dat depthconv_line.dat qconv_line.dat tconv_line.dat

done

mv mco.dat phase.dat mhe.dat lumi.dat  mass.dat  radius.dat  time.dat  rhe.dat rco.dat inertia.dat hsup.dat hesup.dat nsup.dat osup.dat csup.dat depthconv.dat qconv.dat tconv.dat ./output/$DIROUT
echo "mv mco.dat phase.dat mhe.dat lumi.dat  mass.dat  radius.dat  time.dat  rhe.dat rco.dat inertia.dat hsup.dat hesup.dat nsup.dat osup.dat csup.dat depthconv.dat qconv.dat tconv.dat ./output/$DIROUT"

done
