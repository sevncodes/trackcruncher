#include <parameters.h>

void parameters::read_param(){
 
    string fname = "./input/parameters.txt";
    in.open(fname.c_str(), ios::in);
    if(!in){ 
        string errmsg = "Cannot read file " + fname;
        throw errhand_macro(errmsg);
    }

    std::cout<< "PARAMS:"<<std::endl;
    
    read_line(&in, &value);
    if(in.eof()) throw errhand_macro("Input parameters file cannot finish here. Missing: \n-lines to ignore, \n-Maximum relative error, \n-Ndigits in output, \n-Code, \n-AGB cut option");
    global::n_ignore = utls.string_to_number<int>(value, &err);
    if(err)  throw errhand_macro("Something went wrong while reading the number of lines to ignore, var:n_ignore");
    if(global::n_ignore <= 0) throw errhand_macro("Total number of lines to ignore cannot be negative");
    cout<<" NIGNORE = "<<global::n_ignore<<endl;

    
    read_line(&in, &value);
    if(in.eof()) throw errhand_macro("Input parameters file cannot finish here. Missing: \n-Maximum relative error, \n-Ndigits in output, \n-Code, \n-AGB cut option");
    global::max_rel_error = utls.string_to_number<double>(value, &err);
    if(err)  throw errhand_macro("Something went wrong while reading the required maximum relative error, var:max_rel_error");
    if(global::max_rel_error <= 0) throw errhand_macro("The maximum relative error cannot be negative");
    cout<<" MAXRE = "<<global::max_rel_error<<endl;

    read_line(&in, &value);
    if(in.eof()) throw errhand_macro("Input parameters file cannot finish here. Missing: \n-Ndigits in output, \n-Code, \n-AGB cut option");
    global::ndigit = utls.string_to_number<int>(value, &err);
    if(err)  throw errhand_macro("Something went wrong while reading the required number of digits in output, var:ndigit");
    if(global::max_rel_error <= 0) throw errhand_macro("The required number od digits cannot be zero or negative");
    cout<<" NDIGIT = "<<global::ndigit<<endl;
    
    read_line(&in, &value);
    if(in.eof()) throw errhand_macro("Input parameters file cannot finish here. Missint: Missing: \n-Code, \n-AGB cut option");
    global::stevocode = value;
    cout<<" CODE = "<<global::stevocode<<endl;
    if(global::stevocode != "parsec" && global::stevocode != "parsec_old" && global::stevocode != "franec" && global::stevocode != "mist")  throw errhand_macro("Unrecognized stellar evolution code");

    read_line(&in, &value);
    if(in.eof()) throw errhand_macro("Input parameters file cannot finish here. Missing: \n-AGB cut option");

    if (value=="yes" or value=="y" or value=="true" or value=="t")
        global::AGB_cut = true;
    else if (value=="no" or value=="n" or value=="false" or value=="f")
        global::AGB_cut = false;
    else
        throw errhand_macro("Unrecognized AGB_cut option (use yes/no or true/false)");
    cout<<" CUT ON AGB? = "<<global::AGB_cut<<endl;




    global::calledParams = true;
    
    
    return;
    
}
